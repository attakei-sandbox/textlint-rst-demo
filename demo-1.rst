今まで「サイトはNetlify」「スライドはGitLab Pages」といった配信分けをしていたのですが、
「 ``/slides`` にビルドしたスライドを置きたい」という動機のものと、こんな構成を検討しています。

* 最初にtextlintで文法チェック
* ドキュメントを別にビルドしてarticle化
* 複数articleを束ねてFirebaseへデプロイ
